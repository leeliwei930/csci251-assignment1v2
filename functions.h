#include <iostream>
#include <vector>
#include <fstream>
#include <string>

using namespace std;
struct Animal {
    int ID;
    string name;
    string type;
    string registration;
    int problem;
};

struct Vet {
    int ID;
    string name;
    int quality;
};

struct Problem {
    int ID;
    string name;
    int problemDetComplx;
    int problemTreatComplx;
    int treatment;
};

struct Treatment {
    int ID;
    string name;
};

struct Clinic {
    vector <Animal> animals;
    vector <Vet> vets;
    vector <Problem> problems;
    vector <Treatment> treatments;
};

struct Report {
    string animalName;
    string animalProblem;
    string animalProblemDetComplx;
    string animalProblemTreatComplx;
    string diagnoseProblem;
    string treatment;
    string vetName;
    string diagnoseRate;
    string diagnoseResult;
    string chance;
};


void validateArguments(int argCount, int expectedArguments);
int getNumbersOfLine(string path);

void preprocessVetsFile(string path);
void preprocessAnimalsFile(string path);
void preprocessTreatmentsFile(string path);
void preprocessProblemsFile(string path);
void preprocessTreatmentsFile(string path);

int diagnosisRate(int vetQuality, int problemDetComplx, int problemTreatComplx);

void assignVetsToAnimals(int animalLimit);

// misc functions 
bool contentValidator(string text);

bool validateContentLength(string text, int requireLength);

int randomRangeGenerator(int min, int max);

void printAnimals();
void printVets();
void printProblems();
void printTreatments();
void printHeader();
void printResult(vector<Report> &reports, int maxRow);
void generateReportFile(vector<Report> &reports, int maxRow);
void writeFile(string path);
