#include <iostream>
#include <random>
#include <cmath>
#include <fstream>
#include <vector>
#include <iomanip>
#include <numeric>
#include "functions.h"
using namespace std;


fstream animalsFile;
fstream vetsFile;
fstream problemsFile;
fstream treatmentsFile;
fstream outputFile;

string outputFileName;
Clinic clinic;
const int maxAnimalsLine = 20;
const int maxVetsLine = 5;
const int maxProblemsLine = 20;
const int maxTreatmentLine = 20;


int totalAnimalsCount = 0;
int totalVetsCount = 0;
int totalProblemsCount = 0;
int totalTreatmentCount = 0;

void writeFile(string path){
    outputFileName = path;
    outputFile.open(path);
    if(!outputFile){
        cerr << "Unable to write the report to file name " << path << " " << endl;
        exit(-1);
    }
}
void validateArguments(int argCount, int expectedArguments){
    if((argCount  - 1)!= expectedArguments){
        cerr << "Expected arguments should be " << expectedArguments;
        cerr << " , " << argCount - 1  << " Given" <<endl;
        cerr << "Exit Program";
        exit(-1);
    }
}
void promptReadFileError(string path){
    cerr << "Unable to read file : " << path;
    cerr << "Exit Program";
    exit(-1);
}


void preprocessAnimalsFile(string path){
    animalsFile.open(path);
    while(!animalsFile){
         promptReadFileError(path);
    }
    char placeholder[100];
    int lineCount = 0;
    int maxLine = (getNumbersOfLine(path) < maxAnimalsLine) ? getNumbersOfLine(path) : maxAnimalsLine;
    Animal animalPlaceholder;
    
    while(lineCount < maxLine){
        animalsFile.getline(placeholder, 100, ':');
        animalPlaceholder.ID = lineCount;
        animalPlaceholder.name = placeholder;
        animalsFile.getline(placeholder, 100, ':');
        animalPlaceholder.type = placeholder;
        animalsFile.getline(placeholder, 100, ':');
        animalPlaceholder.registration = placeholder;
        animalsFile.getline(placeholder, 100, '.');
        animalPlaceholder.problem =  atoi(placeholder);
        clinic.animals.push_back(animalPlaceholder);
        animalsFile.getline(placeholder, 100, '\n');

        lineCount++;
        totalAnimalsCount++;
    }
    
    animalsFile.close();
}

void preprocessVetsFile(string path){
    vetsFile.open(path);

    if(!vetsFile){
        promptReadFileError(path);
    }
    char placeholder[100];
    int lineCount = 0;
    int maxLine = (getNumbersOfLine(path) < maxVetsLine) ? getNumbersOfLine(path) : maxVetsLine;
    Vet vetPlaceholder;
    
    while(lineCount < maxLine){
        vetsFile.getline(placeholder, 100, ':');
        vetPlaceholder.ID = lineCount;
        vetPlaceholder.name = placeholder;
        vetsFile.getline(placeholder, 100, '.');
        vetPlaceholder.quality = atoi(placeholder);
        clinic.vets.push_back(vetPlaceholder);
        vetsFile.ignore(1);
        lineCount++;
        totalVetsCount++;
    }
    vetsFile.close();
}

void preprocessProblemsFile(string path){
    problemsFile.open(path);
    if(!problemsFile){
        promptReadFileError(path);
    }

    char placeholder[100];
    int lineCount = 0;
    int maxLine =  (getNumbersOfLine(path) < maxProblemsLine) ? getNumbersOfLine(path) : maxProblemsLine;
    Problem problemplaceholder;
    while(lineCount < maxLine){
        problemsFile.getline(placeholder, 100, ':');
        problemplaceholder.ID = lineCount;
        problemplaceholder.name = placeholder;
        problemsFile.getline(placeholder, 100, ':');
        problemplaceholder.problemDetComplx = atoi(placeholder);
        problemsFile.getline(placeholder, 100, ':');
        problemplaceholder.problemTreatComplx = atoi(placeholder);
        problemsFile.getline(placeholder, 100, '.');
        problemplaceholder.treatment = atoi(placeholder);
        clinic.problems.push_back(problemplaceholder);
        problemsFile.ignore(1);
        lineCount++;
        totalProblemsCount++;
    }
    problemsFile.close();
}

void preprocessTreatmentsFile(string path){
    treatmentsFile.open(path);
    if(!treatmentsFile){
        promptReadFileError(path);
    }
    Treatment treatmentPlaceholder;

    char placeholder[100];
    int lineCount = 0;
    int maxLine = (getNumbersOfLine(path) < maxTreatmentLine) ? getNumbersOfLine(path) : maxTreatmentLine;
    while(lineCount < maxLine){
        treatmentsFile.getline(placeholder, 100, '.');
        treatmentPlaceholder.ID = lineCount;
        treatmentPlaceholder.name = placeholder;
        
        clinic.treatments.push_back(treatmentPlaceholder);
        treatmentsFile.ignore(1);
        lineCount++;
        totalTreatmentCount++;
    }
    treatmentsFile.close();
}


bool contentValidator(string text){
    return !text.empty();
}

bool validateContentLength(string text, int requireLength){
    int textLength = 0;
    for(char c : text){
        textLength++;
    }
    return textLength == requireLength;
}

int getNumbersOfLine(string path){
    fstream file;
    file.open(path);
    char placeholder[100];
    int lineCount = 0;
    while(!file.eof()){
        file.getline(placeholder, 100);
        if(placeholder[0] != '\0'){
            lineCount ++;
        }
    }
    return lineCount;
}

void printAnimals(){
    for(auto animalRec : clinic.animals){
        // cout << "ID" << animalRec.ID << endl;

        cout << "Name" << animalRec.name;
        // cout << "Type" << animalRec.type << endl;
        cout << "Problem" << animalRec.problem;
    }
}

void printVets(){
    for(auto vetRec : clinic.vets){
        cout << "ID" << vetRec.ID << endl;
        cout << "Name" << vetRec.name << endl;
        cout << "Quality" << vetRec.quality << endl;
    }
}

void printProblems(){
    for(auto problemRec : clinic.problems){
        cout << "ID" << problemRec.ID << endl;
        cout << "Name " << problemRec.name << endl;
        cout << "Problem Determination Complexity" <<  problemRec.problemDetComplx << endl;
        cout << "Problem Treatment Complexity" << problemRec.problemTreatComplx << endl;
        cout << "Treatment" << problemRec.treatment << endl;
    }
}

void printTreatments(){
    for(auto treatmentRec : clinic.treatments){
        cout << "ID" << treatmentRec.ID << endl;
        cout << "Name" << treatmentRec.name << endl;
    }
}

int diagnosisRate(int vetQuality, int problemDetComplx, int problemTreatComplx){
    if(vetQuality == problemDetComplx){
        // if the vetquality is same as the problem determination complexitiy
        // chance is 50% diagnose rate and deduct a problemTreatComplx / 10
        return 50 - (problemTreatComplx / 10);
    } else if (vetQuality > problemDetComplx){
        // if the vetquality is greater than the problem deteriniation complexity
        // chance of 60% can be correctly diagnose and deduct with a treatmentcomplexity divide by 10
        return 60 - (problemTreatComplx / 10);
    } else {

        // if problem detcomplexity is greater than the quality, lowest chance of diagnosis rate accurancy
        // with the chance of 40% deduct  with problemTreatComplx divide by 10
        return 40 - (problemTreatComplx / 10);
    }
}

string result(int diagnoseRate, int chanceRate){
    return (chanceRate < diagnoseRate) ? "success" : "failed";
}

int randomRangeGenerator(int min, int max){
    random_device rand;
    return  (min + (rand() % (int)(max - min + 1)));
    
}

void assignVetsToAnimals(int animalLimit){
    
    vector <Report> reports(totalAnimalsCount);
    int randomVetIndex;
    int counter = 0;
    if(animalLimit > totalAnimalsCount){
        // if user input more limit than it provided in the file
        animalLimit = totalAnimalsCount;
    }
    printHeader();

    while (counter < animalLimit){
        randomVetIndex = randomRangeGenerator(0, totalVetsCount - 1);
        Vet randomVet = clinic.vets[randomVetIndex];
        Animal animalRec = clinic.animals[counter];
        Problem animalProb = clinic.problems[animalRec.problem - 1];
        int randomProblemIndex;
        int chance = randomRangeGenerator(0,100);
        int diagnoseRate = diagnosisRate(
            randomVet.quality,
            animalProb.problemDetComplx,
            animalProb.problemTreatComplx
        );

        reports[counter].animalName = animalRec.name;
        reports[counter].animalProblem = animalProb.name;
        reports[counter].animalProblemDetComplx = to_string(animalProb.problemDetComplx);
        reports[counter].animalProblemTreatComplx = to_string(animalProb.problemTreatComplx);
        reports[counter].vetName = randomVet.name;
        reports[counter].chance = to_string(chance);
        reports[counter].diagnoseRate =  to_string(diagnoseRate);
        if (diagnoseRate > chance){
            // success diagnose, use to specific treatment
            reports[counter].diagnoseProblem = animalProb.name;
            reports[counter].treatment =  clinic.treatments[animalRec.problem - 1].name;
            reports[counter].diagnoseResult = result(diagnoseRate, chance);
        } else {
            // failed to diagnose, pick a random problem with associated treatment
             randomProblemIndex = randomRangeGenerator(0, totalProblemsCount - 1);
            Problem randomProblem = clinic.problems[randomProblemIndex];
            Treatment problemTreatment = clinic.treatments[randomProblemIndex];

            reports[counter].diagnoseProblem =  randomProblem.name;
            reports[counter].treatment = problemTreatment.name;
            reports[counter].diagnoseResult = result(diagnoseRate, chance);
        }
        printResult(reports, counter);
        counter ++;
    }
    generateReportFile(reports, animalLimit);


}

void printResult(vector<Report> &reports, int row){
    cout<<left<<setw(25)<<reports[row].animalName
            <<left<<setw(25)<<reports[row].animalProblem
            <<left<<setw(25)<<reports[row].vetName
            <<left<<setw(25)<<reports[row].diagnoseProblem
            <<left<<setw(25)<<reports[row].treatment
            <<left<<setw(25)<<reports[row].diagnoseRate
            <<left<<setw(25)<<reports[row].chance
            <<left<<setw(25)<<reports[row].diagnoseResult
            <<endl;
    
}

void printHeader(){
    cout<<left<<setw(25)<<"Animal Name"
        <<left<<setw(25)<<"Problem"
        <<left<<setw(25)<<"Vet Name"
        <<left<<setw(25)<<"Diagnose"
        <<left<<setw(25)<<"Treatment"
        <<left<<setw(25)<<"Diagnose Rate"
        <<left<<setw(25)<<"Chance"
        <<left<<setw(25)<<"Diagnose Result"
        <<endl;
}   

void generateReportFile(vector<Report> &reports, int maxLine){
 
        outputFile << left<<setw(25)<<"Animal Name"
        <<left<<setw(25)<<"Problem"
        <<left<<setw(25)<<"Vet Name"
        <<left<<setw(25)<<"Diagnose"
        <<left<<setw(25)<<"Treatment"
        <<left<<setw(25)<<"Diagnose Rate"
        <<left<<setw(25)<<"Chance"
        <<left<<setw(25)<<"Diagnose Result"
        <<endl;
        
        for(int counter = 0; counter < maxLine; counter ++){
            outputFile<<left<<setw(25)<<reports[counter].animalName
            <<left<<setw(25)<<reports[counter].animalProblem
            <<left<<setw(25)<<reports[counter].vetName
            <<left<<setw(25)<<reports[counter].diagnoseProblem
            <<left<<setw(25)<<reports[counter].treatment
            <<left<<setw(25)<<reports[counter].diagnoseRate
            <<left<<setw(25)<<reports[counter].chance
            <<left<<setw(25)<<reports[counter].diagnoseResult
            <<endl;
        }

        outputFile.close();
        
}
