
#include <iostream>
#include "functions.h"

using namespace std;
int animalLimit = 0;



int main(int argc, char *argv[]){
    validateArguments(argc,6);
    animalLimit = atoi(argv[1]);
    // readFile(vetsFile, argv[3]);
    // readFile(problemsFile, argv[4]);
    // readFile(treatmentsFile, argv[5]);
    // writeFile(outputFile, argv[6]);
    preprocessAnimalsFile(argv[2]);
    preprocessVetsFile(argv[3]);
    preprocessProblemsFile(argv[4]);
    preprocessTreatmentsFile(argv[5]);
    writeFile(argv[6]);
    
    // printAnimals();

    assignVetsToAnimals(animalLimit);
    // animalsFile.close();
    // vetsFile.close();
    // problemsFile.close();
    // treatmentsFile.close();
    // outputFile.close();
    
    return 0;
}
